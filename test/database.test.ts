import { expect } from "chai";
import DatabaseFactory from "../src/database/databaseFactory";
import IDatabase from "../src/database/IDatabase";

beforeAll(async () => {
    DatabaseFactory.enableTestMode();
});

test('Get Project', async () => {

    const db: IDatabase = DatabaseFactory.getInstance();

    const id = "a";

    expect(await db.getProject(id)).to.deep.equal({
        id,
        name: "Complete Church Wall",
        budget: 7000,
        goal: "The church wall is only partially built. People are coming into the church property and vandalising the building. Completing the church wall is an important first step before improving the church building."
    });

});

test('List Projects', async () => {

    const db: IDatabase = DatabaseFactory.getInstance();

    expect(await db.listProjects()).to.deep.equal([{
        id: "a",
        name: "Complete Church Wall",
        budget: 7000,
        goal: "The church wall is only partially built. People are coming into the church property and vandalising the building. Completing the church wall is an important first step before improving the church building."
    }]);

});

test('Create Project', async () => {

    const db: IDatabase = DatabaseFactory.getInstance();

    expect(await db.upsertProject({
        id: "a",
        name: "Complete Church Wall",
        budget: 7000,
        goal: "The church wall is only partially built. People are coming into the church property and vandalising the building. Completing the church wall is an important first step before improving the church building."
    })).to.deep.equal({
        id: "a",
        name: "Complete Church Wall",
        budget: 7000,
        goal: "The church wall is only partially built. People are coming into the church property and vandalising the building. Completing the church wall is an important first step before improving the church building."
    });

});

test('Update Project', async () => {

    const db: IDatabase = DatabaseFactory.getInstance();

    const id = "a";

    expect(await db.upsertProject({
        id: "a",
        name: "Complete Church Wall",
        budget: 7000,
        goal: "The church wall is only partially built. People are coming into the church property and vandalising the building. Completing the church wall is an important first step before improving the church building."
    })).to.deep.equal({
        id: "a",
        name: "Complete Church Wall",
        budget: 7000,
        goal: "The church wall is only partially built. People are coming into the church property and vandalising the building. Completing the church wall is an important first step before improving the church building."
    });

});

test('Delete Project', async () => {

    const db: IDatabase = DatabaseFactory.getInstance();

    const id = "a";

    expect(await db.deleteProject(id)).to.deep.equal({
        id,
        name: "Complete Church Wall",
        budget: 7000,
        goal: "The church wall is only partially built. People are coming into the church property and vandalising the building. Completing the church wall is an important first step before improving the church building."
    });

});
