import { lambdaHandler } from "../src/lambda";
import DatabaseFactory from "../src/database/databaseFactory";
import { APIGatewayProxyResult, APIGatewayProxyEvent } from "aws-lambda";
import { expect } from "chai";

const requestTemplate: APIGatewayProxyEvent = {
    resource: null,
    path: null,
    httpMethod: null,
    headers: {},
    multiValueHeaders: {},
    queryStringParameters: null,
    multiValueQueryStringParameters: null,
    pathParameters: null,
    stageVariables: null,
    requestContext: {
        resourceId: null,
        resourcePath: null,
        httpMethod: null,
        extendedRequestId: null,
        requestTime: null,
        path: null,
        accountId: null,
        stage: null,
        requestTimeEpoch: null,
        requestId: null,
        identity:
        {
            cognitoIdentityPoolId: null,
            accountId: null,
            cognitoIdentityId: null,
            caller: null,
            sourceIp: null,
            apiKey: null,
            apiKeyId: null,
            accessKey: null,
            cognitoAuthenticationType: null,
            cognitoAuthenticationProvider: null,
            userArn: null,
            userAgent: null,
            user: null
        },
        domainName: null,
        apiId: null
    },
    body: null,
    isBase64Encoded: false
};

beforeAll(async () => {
    DatabaseFactory.enableTestMode();
});

test('Invoke lambda hanlder', async () => {

    const request = { 
        ...requestTemplate, 
        body: JSON.stringify({ projectId: "1" }) 
    }

    const expectedResponse: APIGatewayProxyResult = {
        statusCode: 200,        
        body: JSON.stringify({
            id: "1",
            name: "Complete Church Wall",
            budget: 7000,
            goal: "The church wall is only partially built. People are coming into the church property and vandalising the building. Completing the church wall is an important first step before improving the church building."
        })
    }

    const actualResponse = await lambdaHandler(request);

    expect(actualResponse).to.deep.equal(expectedResponse);

});

test('Handle invalid inputs', async () => {

    const request = { 
        ...requestTemplate, 
        body: JSON.stringify({ }) 
    }

    const expectedResponse: APIGatewayProxyResult = {
        statusCode: 400,        
        body: JSON.stringify({
            error: "body.projectId is null"
        })
    }

    const actualResponse = await lambdaHandler(request);

    expect(actualResponse).to.deep.equal(expectedResponse);

});