# ####################################################################################################################################
# This script builds, tests and deploys a Lambda Function. 
#
# It requires a $LAMBDA_S3_BUCKET enironment variable to be set. See setenv.sh.
# ####################################################################################################################################

# Make sure to compile before deploy
rm -r dist/ && npm run build && \
# TODO: Test before deploy
npm test && \
# Remove development dependencies
rm -r node_modules && npm i --production --loglevel=error && \
# Package the Lambda Function and upload to S3 folder
aws cloudformation package --template-file template.yaml --s3-bucket $LAMBDA_S3_BUCKET --output-template-file template.packaged.yaml && \
# Deploy the Lambda Function
aws cloudformation deploy --template-file template.packaged.yaml --stack-name $STACK_NAME --capabilities CAPABILITY_IAM
# Reinstall development dependencies
npm install --loglevel=error 