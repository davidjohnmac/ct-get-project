import DatabaseFactory from "./database/databaseFactory";
import IDatabase from "./database/IDatabase";

(async function() {

    try {

        // DatabaseFactory.enableTestMode();

        const db: IDatabase = DatabaseFactory.getInstance();

        // await db.upsertProject({
        //     id: "1",
        //     name: "Complete Church Wall",
        //     budget: 7000,
        //     goal: "The church wall is only partially built. People are coming into the church property and vandalising the building. Completing the church wall is an important first step before improving the church building."
        // });

        // await db.upsertProject({
        //     id: "2",
        //     name: "Complete Church Toilet",
        //     budget: 7000,
        //     goal: "The church has a small two stall toliet structure but no toilet or plumbing."
        // });

        // await db.upsertProject({
        //     id: "3",
        //     name: "Complete Church Wall",
        //     budget: 7000,
        //     goal: "The church wall is only partially built. People are coming into the church property and vandalising the building. Completing the church wall is an important first step before improving the church building."
        // });

        console.log(await db.deleteProject("1"));

    } catch(err) {
        console.error(err);
    }

})();

