import IDatabase from "./IDatabase";
import { DynamoDB } from "aws-sdk";
import { Project } from "../model/Project";

export default class RealDatabase implements IDatabase {
    
    private db: DynamoDB.DocumentClient;

    constructor() {
        this.db = new DynamoDB.DocumentClient({ region: process.env.AWS_DEFAULT_REGION });
    }

    async getProject(id: string): Promise<Project> {
        return new Promise<Project>((resolve, reject) => {
            
            const request = this.db.get({
                TableName: "project",                
                Key: { id }
            }, (err, data) => {
                
                if (err) {
                    reject(new Error(`Error getting project from DynamoDB: ${err.message}`));
                } else {
                    resolve(data.Item as Project);
                }
    
            });    

        });

    }

    async listProjects(): Promise<Project[]> {
        return new Promise<Project[]>((resolve, reject) => {
            
            const request = this.db.scan({
                TableName: "project"
            }, (err, data) => {
                
                if (err) {
                    reject(new Error(`Error getting project from DynamoDB: ${err.message}`));
                } else {                    
                    resolve(data.Items as Project[]);
                }
    
            });    

        });
    }
    
    async upsertProject(newProject: Project): Promise<Project> {
        return new Promise<Project>((resolve, reject) => {
            
            const request = this.db.put({
                TableName: "project",                
                Item: newProject
            }, (err, data) => {
                
                if (err) {
                    reject(new Error(`Error putting project to DynamoDB: ${err.message}`));
                } else {
                    resolve(newProject);
                }
    
            });    

        });
    }

    async deleteProject(id: string): Promise<Project> {
        return new Promise<Project>((resolve, reject) => {
            
            const request = this.db.delete({
                TableName: "project",  
                ReturnValues: "ALL_OLD",              
                Key: { id }
            }, (err, data) => {
                
                if (err) {
                    reject(new Error(`Error putting project to DynamoDB: ${err.message}`));
                } else {
                    resolve(data.Attributes as Project);
                }
    
            });    

        });
    }

}