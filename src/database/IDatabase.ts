import { Project } from "../model/Project";

export default interface IDatabase {

    getProject(id: string): Promise<Project>;
    listProjects(): Promise<Project[]>;
    upsertProject(project: Project): Promise<Project>;
    deleteProject(id: string): Promise<Project>;

}