import IDatabase from "./IDatabase";
import MockDatabase from "./MockDatabase";
import RealDatabase from "./RealDatabase";

export default class DatabaseFactory {

    private static instance: IDatabase;

    public static enableTestMode() {
        this.instance = new MockDatabase();
    }

    public static getInstance() {
        if (!this.instance) {
            this.instance = new RealDatabase();
        }
        return this.instance;
    }

}