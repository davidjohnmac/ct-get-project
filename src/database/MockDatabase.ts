import IDatabase from "./IDatabase";
import { Project } from "../model/Project";

export default class MockDatabase implements IDatabase {

    async getProject(id: string): Promise<Project> {

        return {
            id,
            name: "Complete Church Wall",
            budget: 7000,
            goal: "The church wall is only partially built. People are coming into the church property and vandalising the building. Completing the church wall is an important first step before improving the church building."
        }

    }

    async listProjects(): Promise<Project[]> {
        return [{
            id: "a",
            name: "Complete Church Wall",
            budget: 7000,
            goal: "The church wall is only partially built. People are coming into the church property and vandalising the building. Completing the church wall is an important first step before improving the church building."
        }];
    }
    async upsertProject(project: any): Promise<Project> {
        
        return project;

    }
    async deleteProject(id: string): Promise<Project> {

        return {
            id: "a",
            name: "Complete Church Wall",
            budget: 7000,
            goal: "The church wall is only partially built. People are coming into the church property and vandalising the building. Completing the church wall is an important first step before improving the church building."
        };
        
    }


}