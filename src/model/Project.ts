export class Project {

    id?: string;
    name: string;
    goal: string;
    budget: number;

}