import IDatabase from "./database/IDatabase";
import DatabaseFactory from "./database/databaseFactory";
import { Project } from "./model/Project";

export default async function run(projectId: string): Promise<Project> {

    const database: IDatabase = DatabaseFactory.getInstance();
    const project: Project = await database.getProject(projectId);
    return project;
    
}
