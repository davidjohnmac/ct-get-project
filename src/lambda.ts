import run from "./main";
import { Project } from "./model/Project";
import { APIGatewayProxyResult, APIGatewayProxyEvent } from "aws-lambda";

export async function lambdaHandler(input: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> {

    let bodyJson;

    try {

        bodyJson = JSON.parse(input.body);

        console.debug("Input", bodyJson);

        validateInput(bodyJson);

    } catch (err) {

        console.error("400", err);
        return {
            statusCode: 400,
            body: JSON.stringify({
                error: err.message
            })
        }

    }

    try {

        const project: Project = await run(bodyJson.projectId);

        return {
            statusCode: 200,
            body: JSON.stringify(project)
        }
    
    } catch(err) {

        console.error("500", err);
        return {
            statusCode: 500,
            body: JSON.stringify({
                error: err.message
            })
        }

    }

}

function validateInput(bodyJson: any) {
    if (!(bodyJson && bodyJson.projectId)) throw new Error("body.projectId is null");
}